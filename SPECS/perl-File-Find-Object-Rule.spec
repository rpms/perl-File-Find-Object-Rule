%if ! (0%{?rhel})
%{bcond_without perl_File_Find_Object_Rule_enables_optional_test}
%else
%{bcond_with perl_File_Find_Object_Rule_enables_optional_test}
%endif

Name:           perl-File-Find-Object-Rule
Version:        0.0306
Release:        6%{?dist}
Summary:        Alternative interface to File::Find::Object
License:        GPL+ or Artistic
URL:            http://search.cpan.org/dist/File-Find-Object-Rule/
Source0:        http://www.cpan.org/authors/id/S/SH/SHLOMIF/File-Find-Object-Rule-%{version}.tar.gz
BuildArch:      noarch
# Module Build
BuildRequires:  coreutils
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(lib)
BuildRequires:  perl(Module::Build)
# Module Runtime
BuildRequires:  perl(Carp)
BuildRequires:  perl(Class::XSAccessor)
BuildRequires:  perl(Cwd)
BuildRequires:  perl(File::Basename)
BuildRequires:  perl(File::Find::Object)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(Number::Compare)
BuildRequires:  perl(strict)
BuildRequires:  perl(Text::Glob)
BuildRequires:  perl(vars)
BuildRequires:  perl(warnings)
# Script Runtime
BuildRequires:  perl(File::Spec::Functions)
# Test Suite
BuildRequires:  perl(File::Path)
BuildRequires:  perl(Test::More)
# Extra Tests
%if 0%{!?perl_bootstrap:1} && %{with perl_File_Find_Object_Rule_enables_optional_test}
BuildRequires:  perl(Test::Pod) >= 1.14
BuildRequires:  perl(Test::Pod::Coverage) >= 1.04
BuildRequires:  perl(Test::TrailingSpace)
%endif
# Dependencies
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

%description
File::Find::Object::Rule is a friendlier interface to File::Find::Object. It 
allows you to build rules that specify the desired files and directories.

%prep
%setup -qn File-Find-Object-Rule-%{version}

%build
perl Build.PL --installdirs=vendor
./Build

%install
./Build install --destdir=%{buildroot} --create_packlist=0
%{_fixperms} %{buildroot}

%check
RELEASE_TESTING=1 ./Build test

%files
%license LICENSE
%doc Changes README
%{_bindir}/findorule
%{perl_vendorlib}/File/
%{_mandir}/man1/findorule.1*
%{_mandir}/man3/File::Find::Object::Rule.3*
%{_mandir}/man3/File::Find::Object::Rule::Extending.3*
%{_mandir}/man3/File::Find::Object::Rule::Procedural.3*

%changelog
* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.0306-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.0306-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jun 07 2017 Jitka Plesnikova <jplesnik@redhat.com> - 0.0306-4
- Perl 5.26 re-rebuild of bootstrapped packages

* Sun Jun 04 2017 Jitka Plesnikova <jplesnik@redhat.com> - 0.0306-3
- Perl 5.26 rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.0306-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Sep 12 2016 Paul Howarth <paul@city-fan.org> - 0.0306-1
- Update to 0.0306
  - Made the trailing space tests RELEASE_TESTING only

* Wed May 18 2016 Jitka Plesnikova <jplesnik@redhat.com> - 0.0305-8
- Perl 5.24 re-rebuild of bootstrapped packages

* Mon May 16 2016 Jitka Plesnikova <jplesnik@redhat.com> - 0.0305-7
- Perl 5.24 rebuild

* Mon Feb 29 2016 Paul Howarth <paul@city-fan.org> - 0.0305-6
- Classify buildreqs by usage
- Make %%files list more explicit
- Use %%license

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.0305-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0305-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 06 2015 Jitka Plesnikova <jplesnik@redhat.com> - 0.0305-3
- Perl 5.22 rebuild

* Fri Aug 29 2014 Jitka Plesnikova <jplesnik@redhat.com> - 0.0305-2
- Perl 5.20 rebuild

* Sun Jun 08 2014 Christopher Meng <rpm@cicku.me> - 0.0305-1
- Update to 0.0305

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0304-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Feb 13 2014 Christopher Meng <rpm@cicku.me> - 0.0304-1
- Update to 0.0304

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0303-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun May 26 2013 Christopher Meng <rpm@cicku.me> - 0.0303-1
- Initial Package.
